
export interface Task {
    title: string;
    status: string;
    id: string;
    $key: any;
  }