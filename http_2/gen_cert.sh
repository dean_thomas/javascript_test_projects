openssl genrsa -out ./privkey.pem 2048
openssl req -new -x509 -sha256 -key ./privkey.pem -out ./cert.pem -days 365 -subj "/CN=fake.example.org"